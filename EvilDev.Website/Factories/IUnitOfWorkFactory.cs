using EvilDev.Website.Repositories;

namespace EvilDev.Website.Factories
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork Create();
    }
}