﻿using System;
using System.Linq;
using EvilDev.Website.Factories;
using EvilDev.Website.Models;

namespace EvilDev.Website.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(IUnitOfWorkFactory factory) : base(factory)
        {
        }

        public User Get(Guid id)
        {
            return Query.FirstOrDefault(x => x.Id == id);
        }

        public User Get(string userName)
        {
            return Query.FirstOrDefault(x => x.UserName == userName);
        }

        public void Create(User user)
        {
            Add(user);
        }

        public bool Any()
        {
            return Query.Any();
        }
    }
}