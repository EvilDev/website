﻿using System;
using EvilDev.Website.Models;

namespace EvilDev.Website.Repositories
{
    public interface IUserRepository
    {
        User Get(Guid id);

        User Get(string userName);
        
        void Create(User user);
        
        bool Any();
    }
}