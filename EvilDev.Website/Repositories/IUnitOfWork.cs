using System.Linq;

namespace EvilDev.Website.Repositories
{
    public interface IUnitOfWork
    {
        void Add<TEntity>(TEntity entity);

        IQueryable<TEntity> Query<TEntity>();

        void Update<TEntity>(TEntity entity);

        void Delete<TEntity>(TEntity entity);
    }
}