using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EvilDev.Website.Models;

namespace EvilDev.Website.Repositories
{
    public interface IPostRepository
    {
        IQueryable<Post> Published();

        void Save(Post post);

        Post Get(Guid id);

        Post Get(string title);

        Post GetByOldTitle(string title);
    }
}