using System;
using System.Linq;
using Raven.Client;

namespace EvilDev.Website.Repositories
{
    public class RavenUnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly IDocumentSession _session;

        public RavenUnitOfWork(IDocumentStore documentStore)
        {
            documentStore.Conventions.AllowQueriesOnId = true;
            _session = documentStore.OpenSession();
        }

        public void Dispose()
        {
            try
            {
                _session.SaveChanges();
            }
            finally
            {
                _session.Dispose();
            }
        }

        public void Add<TEntity>(TEntity entity)
        {
            _session.Store(entity);
        }

        public IQueryable<TEntity> Query<TEntity>()
        {
            return _session.Query<TEntity>();
        }

        public void Update<TEntity>(TEntity entity)
        {
            _session.Store(entity);
        }

        public void Delete<TEntity>(TEntity entity)
        {
            _session.Delete(entity);
        }
    }
}