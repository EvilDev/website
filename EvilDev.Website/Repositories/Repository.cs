using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EvilDev.Website.Factories;

namespace EvilDev.Website.Repositories
{
    public abstract class Repository<TEntity>
    {
        private readonly IUnitOfWorkFactory _factory;

        protected Repository(IUnitOfWorkFactory factory)
        {
            _factory = factory;
        }

        protected void Add(TEntity entity)
        {
            Uow().Add(entity);
        }

        protected void Update(TEntity entity)
        {
            Uow().Update(entity);
        }

        protected void Delete(TEntity entity)
        {
            Uow().Delete(entity);
        }

        protected IQueryable<TEntity> Query
        {
            get { return Uow().Query<TEntity>(); }
        }

        protected IUnitOfWork Uow()
        {
            return _factory.Create();
        }
    }
}