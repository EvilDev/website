using System;
using System.Linq;
using System.Threading.Tasks;
using Db4objects.Db4o;
using Db4objects.Db4o.Linq;

namespace EvilDev.Website.Repositories
{
    public class Db4OUnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly IObjectContainer _container;

        public Db4OUnitOfWork(IObjectServer server)
        {
            _container = server.OpenClient();
        }

        public void Add<TEntity>(TEntity entity)
        {
            _container.Store(entity);
        }

        public IQueryable<TEntity> Query<TEntity>()
        {
            return _container.AsQueryable<TEntity>();
        }

        public void Update<TEntity>(TEntity entity)
        {
            _container.Store(entity);
        }

        public void Delete<TEntity>(TEntity entity)
        {
            _container.Delete(entity);
        }

        public void Dispose()
        {
            try
            {
                _container.Commit();
            }
            catch (Exception)
            {
                _container.Rollback();
            }
            finally
            {
                _container.Dispose();
            }
        }
    }
}