using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EvilDev.Website.Extensions;
using EvilDev.Website.Factories;
using EvilDev.Website.Models;

namespace EvilDev.Website.Repositories
{
    public class PostRepository : Repository<Post>, IPostRepository
    {
        public PostRepository(IUnitOfWorkFactory factory) : base(factory)
        {
        }

        public IQueryable<Post> Published()
        {
            return Query.Where(x => x.PublishDate.HasValue);
        }

        public void Save(Post post)
        {
            Add(post);
        }

        public Post Get(Guid id)
        {
            return Query.FirstOrDefault(p => p.Id == id);
        }

        public Post Get(string title)
        {
            var urlCode = title.Replace(' ', '-').RemoveSpecialCharacters();
            return Query.FirstOrDefault(p => p.UrlCode == urlCode);
        }

        public Post GetByOldTitle(string title)
        {
            var urlCode = title.Replace(' ', '-').RemoveSpecialCharacters();
            return Query.FirstOrDefault(
                    p => p.OldTitles.Select(x => x.Replace(' ', '-').RemoveSpecialCharacters()).Contains(urlCode));
        }
    }
}