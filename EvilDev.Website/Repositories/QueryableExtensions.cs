using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EvilDev.Website.Repositories
{
    public static class QueryableExtensions
    {
        public static Task<IEnumerable<T>> ExecuteAsync<T>(this IQueryable<T> query)
        {
            return Task.Run(() => query.ToList().AsEnumerable());
        }
    }
}