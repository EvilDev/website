﻿using System;
using System.Linq;
using System.Threading.Tasks;
using EvilDev.Website.Extensions;
using EvilDev.Website.Models;
using EvilDev.Website.Repositories;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses;
using Nancy.Security;

namespace EvilDev.Website.Modules
{
    public class BlogModule : NancyModule
    {

        public BlogModule(IPostRepository posts)
            : base("blog")
        {
            Get["/"] = parameters => View["index"];

            Get["/{title}"] = parameters =>
            {
                var requestedPost = parameters.title as string;

                var post = posts.Get(requestedPost);

                if (post == null)
                {
                    post = posts.GetByOldTitle(requestedPost);
                    if (post != null)
                    {
                        // We want to permanently redirect old urls to prevent search engine spam.
                        return Response.AsRedirect(string.Format("~/blog/{0}", post.UrlCode),
                            RedirectResponse.RedirectType.Permanent);
                    }
                    
                    return new NotFoundResponse();
                }

                return View["post", post];
            };

            Get["/post"] = parameters =>
            {
                Guid id = parameters.Id;
                return Response.AsJson(posts.Get(id));
            };

            Get["/Posts"] = parameters =>
            {
                var results = posts.Published()
                                         .OrderByDescending(x => x.PublishDate)
                                         .Take(5);
                return Response.AsJson(results);
            };

            Post["/Posts"] = parameters =>
            {
                this.RequiresAuthentication();
                var post = this.Bind<Post>();

                post.CreatedBy = string.Format("{0} {1}", this.User().FirstName, this.User().LastName);
                post.PublishDate = DateTime.UtcNow;

                posts.Save(post);

                return Response.AsJson(post);
            };

            Put["/Posts"] = parameters =>
            {
                this.RequiresAuthentication();
                var fromBody = this.Bind<Post>();

                var post = posts.Get(fromBody.Id);

                if (post.Title != fromBody.Title)
                {
                    var oldTitles = post.OldTitles.ToList();
                    oldTitles.Add(post.Title);
                    post.OldTitles = oldTitles;
                    post.Title = fromBody.Title;
                }

                post.Content = fromBody.Content;

                posts.Save(post);
                
                return Response.AsJson(post);
            };
        }
    }
}