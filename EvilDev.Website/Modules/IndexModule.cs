﻿using Nancy;

namespace EvilDev.Website.Modules
{
    public class IndexModule : NancyModule
    {
        public IndexModule()
        {
            Get["/"] = parameters => View["home/index"];
        }
    }
}