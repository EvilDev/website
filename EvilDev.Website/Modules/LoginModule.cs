﻿using System;
using EvilDev.Website.Models;
using EvilDev.Website.Repositories;
using EvilDev.Website.Resources;
using Nancy;
using Nancy.Authentication.Forms;
using Nancy.ModelBinding;

namespace EvilDev.Website.Modules
{
    public class LoginModule : NancyModule
    {
        public LoginModule(IUserRepository users)
        {
            Get["/login"] = parameters => View["index"];

            Get["/logout"] = parameters =>
            {
                return this.Logout("~/");
            };

            Post["/login"] = parameters =>
            {
                var loginParams = this.Bind<LoginParams>();

                var user = users.Get(loginParams.UserName);

                if (user != null && user.Password == loginParams.Password)
                {
                    return this.Login(user.Id);
                }

                return Response.AsJson(new
                {
                    Message = ErrorMessages.InvalidUsernameOrPassword,
                }, HttpStatusCode.BadRequest);
            };

            Get["/install"] = parameters =>
            {
                if (!users.Any())
                {
                    return View["install"];
                }

                return Response.AsRedirect("~");
            };

            Post["/install"] = parameters =>
            {
                var user = this.Bind<User>();

                user.Id = Guid.NewGuid();

                users.Create(user);

                return Response.AsJson("Success");
            };
        }
    }
}