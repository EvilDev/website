using System;

namespace EvilDev.Website.Models
{
    public class Comment
    {
        public string CreatedBy { get; set; }

        public DateTime LastUpdated { get; set; }

        public string Content { get; set; }
    }
}