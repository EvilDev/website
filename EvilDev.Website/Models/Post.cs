﻿using System;
using System.Collections.Generic;
using EvilDev.Website.Extensions;
using Nancy.Json;

namespace EvilDev.Website.Models
{
    public class Post
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string UrlCode
        {
            get { return Title.Replace(' ', '-').RemoveSpecialCharacters(); }
        }

        [ScriptIgnore]
        public IEnumerable<string> OldTitles { get; set; }

        public string Content { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? PublishDate { get; set; }
    }
}