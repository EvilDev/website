﻿using System;
using System.Collections.Generic;
using Nancy.Security;

namespace EvilDev.Website.Models
{
    public class User : IUserIdentity
    {
        public virtual Guid Id { get; set; }

        public virtual string FirstName { get; set; }

        public virtual string LastName { get; set; }

        public virtual string UserName { get; set; }

        public virtual IEnumerable<string> Claims { get; set; }
        
        public virtual string Password { get; set; }
    }
}