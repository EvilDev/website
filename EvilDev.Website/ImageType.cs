﻿namespace EvilDev.Website
{
    public enum ImageType
    {
        JPG,
        PNG,
        GIF
    }
}