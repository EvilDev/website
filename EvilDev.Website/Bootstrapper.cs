﻿using System.Runtime.Remoting.Contexts;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Db4objects.Db4o;
using EvilDev.Website.Extensions;
using EvilDev.Website.Repositories;
using Nancy;
using Nancy.Authentication.Forms;
using Nancy.Bootstrapper;
using Nancy.Bootstrappers.Windsor;
using Nancy.Extensions;
using Nancy.Responses;

namespace EvilDev.Website
{
    public class Bootstrapper : WindsorNancyBootstrapper
    {
        protected override void ConfigureApplicationContainer(IWindsorContainer existingContainer)
        {
            base.ConfigureApplicationContainer(existingContainer);

            existingContainer.Install(FromAssembly.InThisApplication());
        }

        protected override void ApplicationStartup(IWindsorContainer container, IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);

            pipelines.OnError += (context, error) =>
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(error);
                return null;
            };

            FormsAuthentication.Enable(pipelines, new FormsAuthenticationConfiguration()
            {
                RedirectUrl = "~/login",
                UserMapper = container.Resolve<IUserMapper>(),
            });
        }

        protected override void RequestStartup(IWindsorContainer container, IPipelines pipelines, NancyContext context)
        {
            pipelines.BeforeRequest += ctx =>
            {
                var users = container.Resolve<IUserRepository>();

                if (!users.Any() && ctx.Request.Path.RemoveSpecialCharacters() != "install")
                {
                    return new RedirectResponse(context.ToFullPath("~/install"));
                }

                return null;
            };

            base.RequestStartup(container, pipelines, context);
        }
    }
}