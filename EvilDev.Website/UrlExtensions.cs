﻿using System.Diagnostics;
using Nancy.ViewEngines.Razor;

namespace EvilDev.Website
{
    public static class UrlExtensions
    {
        public static string Stylesheet<TModel>(this UrlHelpers<TModel> urlHelper, string name)
        {
            var suffix = ".css";
            // var suffix = Debugger.IsAttached ? ".css" : ".min.css";

            return urlHelper.Content(string.Format("~/Content/styles/{0}{1}", name, suffix));
        }

        public static string Script<TModel>(this UrlHelpers<TModel> urlHelper, string name, string version = "")
        {
            // var suffix = Debugger.IsAttached ? ".js" : ".min.js";

            var suffix = ".js";

            version = string.IsNullOrWhiteSpace(version) ? string.Empty : string.Format("-{0}", version);

            return urlHelper.Content(string.Format("~/Content/scripts/{0}{1}{2}", name, version, suffix));
        }

        public static string Image<TModel>(this UrlHelpers<TModel> urlHelper, string name, ImageType type = ImageType.PNG)
        {
            return urlHelper.Content(string.Format("~/Content/images/{0}.{1}", name, type));
        }
    }
}