﻿using System.IO;
using System.Web;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Db4objects.Db4o;
using Db4objects.Db4o.CS;
using EvilDev.Website.Repositories;
using Raven.Client;
using Raven.Client.Document;

namespace EvilDev.Website.Installers
{
    public class DataAccessInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
#if DEBUG
            container.Register(Component.For<IUnitOfWork>()
                                        .ImplementedBy<Db4OUnitOfWork>()
                                        .LifestylePerWebRequest());
            
            container.Register(Component.For<IObjectServer>()
                                        .UsingFactoryMethod(() =>
                                        {
                                            var rootPath = HttpContext.Current.Server.MapPath("~");
                                            return Db4oClientServer.OpenServer(Db4oClientServer.NewServerConfiguration(),
                                                Path.Combine(rootPath, "blogposts.data"), 0);
                                        })
                                        .LifestyleSingleton());
#else

            container.Register(Component.For<IUnitOfWork>()
                .ImplementedBy<RavenUnitOfWork>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IDocumentStore>()
                .UsingFactoryMethod(() =>
                {
                    var ravenStore = new DocumentStore
                    {
                        ConnectionStringName = "RAVENHQ_CONNECTION_STRING"
                    };
                    ravenStore.Initialize();
                    return ravenStore;
                }).LifestyleSingleton());
#endif

            container.Register(Classes.FromThisAssembly()
                .Where(t => t.Name.EndsWith("Repository"))
                .WithServiceDefaultInterfaces());
        }
    }
}