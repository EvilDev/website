﻿# CoffeeScript
evildev.controller("BlogController", ($scope, $sce, $http) -> 
    init = -> 
        $http.get('blog/posts').success((data) ->
            $scope.model = data
            $($scope.model).each((post) -> 
                post.editing = false;
                post.tags = [];
            )
        )
        $scope.editing = false;
    
    $scope.Markdown = (str) ->
        converter = new Showdown.converter();
        html = $('<div>' + converter.makeHtml(str) + '</div>')
        $(html).find('pre code')
               .each((i, el) -> 
                    hljs.highlightBlock(el)
               )
        $sce.trustAsHtml($(html).html())
        
    $scope.captureTab = (e) ->
        if(e.keyCode == 9)
            textArea = $(e.target)
            start = textArea[0].selectionStart
            end = textArea[0].selectionEnd
            
            $this = $(textArea)
            value = $this.val()
            
            $this.val(value.substring(0, start) + "\t" + value.substring(end));
            textArea[0].selectionStart = textArea[0].selectionEnd = start + 1
            
            e.preventDefault()
            
    $scope.CreateNew = () ->
        $scope.model.unshift({
            title: "",
            content: "",
            createdby: "",
            editing: true,
            isNew: true
        })
        $scope.editing = true;
    
    $scope.editPost = (post) ->
        post.editing = true;
        $scope.editing = true;
    
    $scope.save = (post) ->
        $scope.saving = true;
        $http.put('blog/posts', post).success((data) ->
            $scope.saving = false;
            $scope.editing = false;
            post = data;
            post.editing = false;
        )
        
    $scope.create = (post) ->
        $scope.saving = true;
        $http.post('blog/posts', post).success((data) ->
            $scope.saving = false;
            $scope.editing = false;
            post = data;
            post.editing = false;
        )
        
    init()
)