﻿evildev.controller("InstallController", ($scope, $sce, $http) ->
    $scope.model = { 
        firstName: "",
        lastName: "",
        userName: "",
        password: ""
    }
    
    $scope.saving = false;
    
    $scope.Install = (user) ->
        $scope.saving = true;
        
        $http.post("/install", $scope.model)
             .success((data) -> 
                window.location.href = "/"
             )
)