// Generated by CoffeeScript 1.7.1
(function() {
  evildev.controller("BlogController", function($scope, $sce, $http) {
    var init;
    init = function() {
      var id;
      id = $('.raw-data').val();
      return $http.get('blog/post?Id=' + id).success(function(data) {
        $scope.post = data;
        return $scope.post.editing = false;
      });
    };
    $scope.Markdown = function(str) {
      var converter, html;
      converter = new Showdown.converter();
      html = $('<div>' + converter.makeHtml(str) + '</div>');
      $(html).find('pre code').each(function(i, el) {
        return hljs.highlightBlock(el);
      });
      return $sce.trustAsHtml($(html).html());
    };
    $scope.captureTab = function(e) {
      var $this, end, start, textArea, value;
      if (e.keyCode === 9) {
        textArea = $(e.target);
        start = textArea[0].selectionStart;
        end = textArea[0].selectionEnd;
        $this = $(textArea);
        value = $this.val();
        $this.val(value.substring(0, start) + "\t" + value.substring(end));
        textArea[0].selectionStart = textArea[0].selectionEnd = start + 1;
        return e.preventDefault();
      }
    };
    $scope.CreateNew = function() {
      $scope.model.unshift({
        title: "",
        content: "",
        createdby: "",
        editing: true,
        isNew: true
      });
      return $scope.editing = true;
    };
    $scope.editPost = function(post) {
      post.editing = true;
      return $scope.editing = true;
    };
    $scope.save = function(post) {
      $scope.saving = true;
      return $http.put('blog/posts', post).success(function(data) {
        $scope.saving = false;
        $scope.editing = false;
        post = data;
        return post.editing = false;
      });
    };
    $scope.create = function(post) {
      $scope.saving = true;
      return $http.post('blog/posts', post).success(function(data) {
        $scope.saving = false;
        $scope.editing = false;
        post = data;
        return post.editing = false;
      });
    };
    return init();
  });

}).call(this);

//# sourceMappingURL=PostController.map
