﻿# CoffeeScript
evildev.controller("LoginController", ($scope, $sce, $http) ->
    $scope.username = ""
    $scope.password = ""
    $scope.error = ""
    $scope.hasError = false;
    
    $scope.saving = false;
    
    $scope.Login =  ->
        $scope.saving = true;
        
        $http.post('Login', { Username: $scope.username, Password: $scope.password}).success((data) ->
            $scope.saving = false;
            window.location.href = "/";
        ).error((data) ->
            $scope.saving = false;
            $scope.error = data.message;
            $scope.hasError = true;
        )
)