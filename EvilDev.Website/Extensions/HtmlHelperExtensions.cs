﻿using EvilDev.Website.Models;
using Nancy;
using Nancy.ViewEngines.Razor;

namespace EvilDev.Website.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static bool IsAuthenticated<T>(this HtmlHelpers<T> html)
        {
            return GetUser(html) != null;
        }

        public static User GetUser<T>(this HtmlHelpers<T> html)
        {
            return html.RenderContext.Context.CurrentUser as User;
        }

        public static User User(this NancyModule module)
        {
            return module.Context.CurrentUser as User;
        }
    }
}