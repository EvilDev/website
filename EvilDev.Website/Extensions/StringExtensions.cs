﻿using System.Linq;
using System.Text;

namespace EvilDev.Website.Extensions
{
    public static class StringExtensions
    {
        public static string RemoveSpecialCharacters(this string str)
        {
            var sb = new StringBuilder();
            foreach (var c in str.Where(c => (c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_' || c == '-'))
            {
                sb.Append(c);
            }
            return sb.ToString();
        }
    }
}