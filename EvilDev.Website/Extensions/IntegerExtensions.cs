using System;
using System.Collections.Generic;

namespace EvilDev.Website.Extensions
{
    public static class IntegerExtensions
    {
        public static IEnumerable<T> Times<T>(this int count, Func<T> func)
        {
            for (var i = 0; i < count; i++)
            {
                yield return func();
            }
        }
    }
}