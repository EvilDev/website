﻿using System;
using EvilDev.Website.Repositories;
using Nancy;
using Nancy.Authentication.Forms;
using Nancy.Security;

namespace EvilDev.Website
{
    public class FormsUserMapper : IUserMapper
    {
        private readonly IUserRepository _userRepository;

        public FormsUserMapper(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public IUserIdentity GetUserFromIdentifier(Guid identifier, NancyContext context)
        {
            return _userRepository.Get(identifier);
        }
    }
}